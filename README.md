# Configuration Laser Jaune

## Octopi

### Configuration octoprint

L'arborescence du dossier configOctopi/octoprint correspond à celle de /home/pi/.octoprint/

- scipts gcode : afterPrintCancelled et afterPrintDone pour éviter que le laser ne se rallume inopinément lors des déplacements après une découpe

### Démarrage automatique 

- Script start_client.sh dans le home
- fichier autostart de start_client dans /home/pi/.config/autostart
