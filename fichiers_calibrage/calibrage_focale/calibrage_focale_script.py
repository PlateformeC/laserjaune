#!/usr/bin/env python

print("(fait a la main en pypypython par Laurent en Glenn)")
print("(decalage en x des lignes de test)")
print("(ajout des commandes M10 et M11)")
print("")
print("G1 F2000 S0.15")
print("")
print("M10")
print("")
for i in range(15): 
    print("G0 X1 Y%s"%(5 + i*2)) 
    print("G0 Z%s"%(i*0.5)) 
    if (i%2 == 0): 
        print("G1 X42") 
    else: 
        print("G1 X38") 
    print("")
print("M11")
