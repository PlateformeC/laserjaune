#!/bin/bash
# This will mount Smoothie, copy Octoprint's upload folder to the SD card, and then unmount.

sudo mount -tvfat -onoexec,nodev,noatime,nodiratime,gid=pi,uid=pi,dmask=0002,fmask=0113 /dev/disk/by-label/LASER /media/LASER
